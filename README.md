# Launch #

```
#!shell
$ workon tcc
$ uwsgi tcc.ini --daemonize /home/ubuntu/logs/tcc.log
```

# Update Supervisor #

```
#!shell

$ sudo supervisorctl reread
hello-celery: available
$ sudo supervisorctl update
hello-celery: added process group
```