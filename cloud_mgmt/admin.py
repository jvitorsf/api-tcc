from django.contrib import admin
from cloud_mgmt.models import Provider, Instance, SecurityGroup, KeyPair, Bill

class ProviderAdmin(admin.ModelAdmin):
    '''
        Admin View for Provider
    '''
    pass


class InstanceAdmin(admin.ModelAdmin):
    '''
        Admin View for Instance
    '''
    pass


class SecurityGroupAdmin(admin.ModelAdmin):
    '''
        Admin View for SecurityGroup
    '''
    pass

class KeyPairAdmin(admin.ModelAdmin):
    '''
        Admin View for KeyPair
    '''
    pass


class BillAdmin(admin.ModelAdmin):
    '''
        Admin View for Bill
    '''
    pass

admin.site.register(Provider, ProviderAdmin)
admin.site.register(Instance, InstanceAdmin)
admin.site.register(SecurityGroup, SecurityGroupAdmin)
admin.site.register(KeyPair, KeyPairAdmin)
admin.site.register(Bill, BillAdmin)