from django.apps import AppConfig


class CloudMgmtConfig(AppConfig):
    name = 'cloud_mgmt'
