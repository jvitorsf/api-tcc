# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-10-16 14:43
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cloud_mgmt', '0008_provider_bucket_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='provider',
            name='account_id',
            field=models.CharField(blank=True, default='', max_length=255),
        ),
    ]
