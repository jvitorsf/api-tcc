from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from boto3.session import Session
from botocore.exceptions import ClientError
from cloud_mgmt import tasks


# https://boto3.readthedocs.io/en/latest/reference/services/ec2.html#client
class Provider(models.Model):
    name = models.CharField(max_length=255, blank=True, null=False, default="")
    account_id = models.CharField(max_length=255, blank=True, default="")
    access_key = models.CharField(max_length=100, blank=False, null=False)
    secret_key = models.CharField(max_length=100, blank=False, null=False)
    bucket_name = models.CharField(max_length=63, blank=True, default='')
    last_connection_status = models.BooleanField(default=False)

    class Meta:
        verbose_name = "Provider"
        verbose_name_plural = "Providers"

    def __str__(self):
        return self.name

    @models.permalink
    def get_absolute_url(self):
        return ('')

    def create_connection(self):
        return Session(
            aws_access_key_id=str(self.access_key),
            aws_secret_access_key=str(self.secret_key),
            region_name='us-east-1'
            )

    @staticmethod
    def extract_csv_fields(fields, row):
        params = {}
        for value_index, value in enumerate(row):
            params[fields[value_index]] = value
        return params

    def get_nodes(self):
        ec2 = self.create_connection().client('ec2')
        instances = ec2.describe_instances()
        
        nodes = []
        for r in instances['Reservations']:
            for i in r['Instances']:
                nodes.append(i)
        return nodes

    def test_connection(self):
        try:
            connection = self.create_connection()
            connection.client('ec2').describe_instances()
            return {'success': True, 'message': "Conexão efetuada com sucesso"}
        except ClientError:
            return {'error': True, 'message': 'Não foi possível estabelecer a conexão com o provedor'}

    # def start_node(self, node):
    #     return tasks.start_instance(self, node)

    # def stop_node(self, node):
    #     return tasks.stop_node(self, node)

    def sync_nodes(self):
        try:
            lib_nodes = self.get_nodes()
            exist_list = []
            for lib_node in lib_nodes:
                if lib_node['State']['Name'] != 'terminated':
                    try:
                        instance = Instance.objects.get(instance_id=lib_node['InstanceId'])
                    except ObjectDoesNotExist:
                        instance = Instance()
                    if 'Tags' in lib_node:
                        for tag in lib_node['Tags']:
                            if tag['Key'] == 'Name':
                                instance.name = tag['Value']
                    instance.instance_id = lib_node['InstanceId']
                    instance.size = lib_node['InstanceType']
                    instance.provider = self
                    instance.save()
                    exist_list.append(instance)

            instance_list = Instance.objects.filter(provider=self)
            for item_to_remove in [item for item in instance_list if item not in exist_list]:
                item_to_remove.delete()

            return {'success': True, 'message': 'Instâncias sincronizadas com sucesso'}
        except ClientError:
            return {'error': True, 'message': "Não foi possível estabelecer uma conexão com o servidor"}

    def sync_security_groups(self):
        try:
            ec2 = self.create_connection().client('ec2')
            security_groups = ec2.describe_security_groups()['SecurityGroups']
            exist_list = []
            for sg in security_groups:
                try:
                    security_group = SecurityGroup.objects.get(security_group_id=sg['GroupId'], provider=self)
                except ObjectDoesNotExist:
                    security_group = SecurityGroup()

                security_group.security_group_id = sg['GroupId']
                security_group.name = sg['GroupName']
                security_group.provider = self
                security_group.description = sg['Description']
                security_group.save()
                exist_list.append(security_group)

            security_group_list = SecurityGroup.objects.filter(provider=self)
            for item_to_remove in [item for item in security_group_list if item not in exist_list]:
                item_to_remove.delete()

            return {'success': True, 'message': 'Grupos de segurança sincronizados com sucesso'}
        except ClientError:
            return {'error': True, 'message': "Não foi possível estabelecer uma conexão com o servidor"}

    def create_instance(self, config):
        try:
            # ubuntu/images/hvm-ssd/ubuntu-trusty-14.04-amd64-server-20140926
            ami = 'ami-0070c468'
            ec2 = self.create_connection().client('ec2')
            security_group = SecurityGroup.objects.get(provider=self, security_group_id=config['security_group'])
            key_pair = KeyPair.objects.get(provider=self, fingerprint=config['key_pair'])
            response = ec2.run_instances(
                InstanceType=config['size_id'],
                ImageId=ami,
                SecurityGroups=[security_group.name],
                KeyName=key_pair.name,
                MinCount=1,
                MaxCount=1)

            if 'Instances' in response:
                instance = Instance()
                instance.instance_id = response['Instances'][0]['InstanceId']
                instance.size = config['size_id']
                instance.provider = self
                instance.name = config['name']
                instance.save()

                ec2.create_tags(Resources=[instance.instance_id], Tags=[{'Key': 'Name', 'Value': instance.name}])
                # waiter = ec2.get_waiter('instance_running')
                # waiter.wait(InstanceIds=[node.instance_id])
                return {'success': True, 'message': 'Provisionando instância'}
            else:
                return {'error': True, 'message': 'Houve um erro ao tentar provisionar a máquina'}
        except BaseException:
            return{'error': True, 'message': 'Houve um erro ao tentar provisionar a máquina'}

    def destroy_instance(self, instance):
        ec2 = self.create_connection().client('ec2')
        response = ec2.terminate_instances(InstanceIds=[instance.instance_id])
        if response['ResponseMetadata']['HTTPStatusCode']:
            instance.delete()
            return {'success': True, 'message': 'Instância excluída'}
        else:
            return {'error': True, 'message': 'Houve um erro ao tentar excluir a instância'}

    def sync_key_pairs(self):
        try:
            ec2 = self.create_connection().client('ec2')
            key_pairs = ec2.describe_key_pairs()['KeyPairs']
            exist_list = []
            for key in key_pairs:
                try:
                    key_pair = KeyPair.objects.get(fingerprint=key['KeyFingerprint'], provider=self)
                except ObjectDoesNotExist:
                    key_pair = KeyPair()

                key_pair.fingerprint = key['KeyFingerprint']
                key_pair.name = key['KeyName']
                key_pair.provider = self
                key_pair.save()
                exist_list.append(key_pair)

            key_pair_list = KeyPair.objects.filter(provider=self)
            for item_to_remove in [item for item in key_pair_list if item not in exist_list]:
                item_to_remove.delete()

            return {'success': True, 'message': 'Chaves sincronizadas com sucesso'}
        except ClientError:
            return {'error': True, 'message': "Não foi possível estabelecer uma conexão com o servidor"}


class Instance(models.Model):
    AVAILABLE_SIZES = (
        ('t2.nano', 't2.nano'),
        ('t2.micro', 't2.micro'),
        ('t2.small', 't2.small'),
        ('t2.medium', 't2.medium'),
        ('t2.large', 't2.large'),
    )

    status = None
    public_ips = []
    private_ips = []
    name = models.CharField(max_length=255, blank=True, null=False, default="")
    size = models.CharField(max_length=100, blank=True, null=True, default=None, choices=AVAILABLE_SIZES)
    instance_id = models.CharField(max_length=255, blank=True, null=True, default=None)
    busy = models.BooleanField(default=False)
    provider = models.ForeignKey(Provider)

    class Meta:
        verbose_name = "Instance"
        verbose_name_plural = "Instances"

    def __str__(self):
        return self.name

    @models.permalink
    def get_absolute_url(self):
        return ('')

    def get_instance_data(self):
        try:
            ec2 = self.provider.create_connection().client('ec2')
            instance = ec2.describe_instances(InstanceIds=[self.instance_id])
            return instance['Reservations'][0]['Instances'][0]
        except KeyError:
            return None

    def update_instance_data(self):
        instance = self.get_instance_data()
        if instance is not None:
            self.public_ips = []
            self.private_ips = []
            public_ip = instance.get('PublicIpAddress') or None
            self.public_ips = [public_ip]
            self.status = instance['State']['Name']

            for network in instance['NetworkInterfaces']:
                self.private_ips.append(network['PrivateIpAddress'])
        return self


class SecurityGroup(models.Model):

    name = models.CharField(max_length=255, blank=True, null=False, default="")
    security_group_id = models.CharField(max_length=100, blank=True, null=True, default=None)
    description = models.TextField(blank=True, null=True, default="")
    provider = models.ForeignKey(Provider)


class KeyPair(models.Model):

    name = models.CharField(max_length=255, blank=True, null=False, default="")
    fingerprint = models.CharField(max_length=255, blank=True, null=True, default=None)
    provider = models.ForeignKey(Provider)


class Bill(models.Model):
    month = models.DateField(blank=True, null=True, default=None)
    provider = models.ForeignKey(Provider, related_name='bill')
    total = models.FloatField(default=0.0)
