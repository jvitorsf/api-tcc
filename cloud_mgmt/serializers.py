from rest_framework import serializers
from cloud_mgmt.models import Provider
from cloud_mgmt.models import Instance
from cloud_mgmt.models import SecurityGroup
from cloud_mgmt.models import KeyPair
from cloud_mgmt.models import Bill


class ProviderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Provider
        fields = ('id', 'name', 'access_key', 'last_connection_status')


class InstanceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Instance
        fields = ('id', 'name', 'status', 'size', 'public_ips', 'private_ips', 'instance_id', 'busy')


class SecurityGroupSerializer(serializers.ModelSerializer):
    provider = ProviderSerializer()

    class Meta:
        model = SecurityGroup
        fields = ('id', 'name', 'security_group_id', 'description', 'provider')


class KeyPairSerializer(serializers.ModelSerializer):
    provider = ProviderSerializer()

    class Meta:
        model = KeyPair
        fields = ('id', 'name', 'fingerprint', 'provider')


class BillSerializer(serializers.ModelSerializer):
    provider = ProviderSerializer()

    class Meta:
        model = Bill
        fields = ('id', 'provider', 'month', 'total')


class ProviderBillSerializer(serializers.Serializer):
    provider = ProviderSerializer()
    bills = BillSerializer(many=True)

    class Meta:
        model = Bill
        fields = ('provider', 'bills')
