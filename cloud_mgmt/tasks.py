from __future__ import absolute_import
from django.core.exceptions import ObjectDoesNotExist
from celery import shared_task
import datetime
import csv
import os


@shared_task
def start_node(node_id):
    from cloud_mgmt.models import Instance
    node = Instance.objects.get(pk=node_id)
    ec2 = node.provider.create_connection().client('ec2')
    response = ec2.start_instances(InstanceIds=[node.instance_id])
    return response


@shared_task
def stop_node(node_id):
    from cloud_mgmt.models import Instance
    node = Instance.objects.get(pk=node_id)
    ec2 = node.provider.create_connection().client('ec2')
    response = ec2.stop_instances(InstanceIds=[node.instance_id])
    return response

@shared_task
def create_instance(name, size_id, pem_key):
    pass


@shared_task
def resize_node(node_id, size_id):
    from cloud_mgmt.models import Instance
    node = Instance.objects.get(pk=node_id)
    node.busy = True
    node.save()
    ec2 = node.provider.create_connection().client('ec2')
    node.update_instance_data()

    restart = False
    if node.status == 'running':
        restart = True
        ec2.stop_instances(InstanceIds=[node.instance_id])
        waiter = ec2.get_waiter('instance_stopped')
        waiter.wait(InstanceIds=[node.instance_id])

    response = ec2.modify_instance_attribute(InstanceId=node.instance_id, Attribute='instanceType', Value=size_id)

    node.size = size_id
    node.save()
    if restart:
        ec2.start_instances(InstanceIds=[node.instance_id])
        waiter = ec2.get_waiter('instance_running')
        waiter.wait(InstanceIds=[node.instance_id])
    node.busy = False
    node.save()

    return response


@shared_task
def fetch_csv_files():
    from cloud_mgmt.models import Provider
    providers = Provider.objects.all()

    for provider in providers:
        s3 = provider.create_connection().resource('s3')
        bucket = s3.Bucket(provider.bucket_name)
        csv_name_pattern = '{0}-aws-billing-csv'.format(provider.account_id)
        for obj in bucket.objects.all():
            if csv_name_pattern in obj.key:
                s3.meta.client.download_file(provider.bucket_name, obj.key, 'assets/billing/' + obj.key)


@shared_task
def update_billing():
    from cloud_mgmt.models import Provider, Bill
    providers = Provider.objects.all()

    fetch_csv_files()

    billing_files = os.listdir('assets/billing')

    for file in billing_files:
        account_id = file.split('-')[0]
        month = int(file.split('-')[-1].split('.')[0])
        year = int(file.split('-')[-2])
        bill_month = datetime.date(year, month, 1)
        provider = Provider.objects.get(account_id=account_id)

        path = 'assets/billing/%s' % file
        with open(path, encoding='utf-8') as bill:
            reader = csv.reader(bill)

            for index, row in enumerate(reader):
                if index == 0:
                    fields = row
                else:
                    extracted_fields = provider.extract_csv_fields(fields, row)
                    if extracted_fields['RecordType'] == 'InvoiceTotal':
                        total = float(extracted_fields['TotalCost'])
                        try:
                            bill = Bill.objects.get(provider=provider, month=bill_month)
                        except ObjectDoesNotExist:
                            bill = Bill()
                        bill.provider = provider
                        bill.month = bill_month
                        bill.total = total
                        bill.save()

