from django.core.exceptions import ObjectDoesNotExist
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import api_view
from cloud_mgmt.serializers import ProviderSerializer, InstanceSerializer,\
    SecurityGroupSerializer, KeyPairSerializer, BillSerializer, ProviderBillSerializer
from cloud_mgmt.models import Provider
from cloud_mgmt.models import Instance
from cloud_mgmt.models import SecurityGroup
from cloud_mgmt.models import KeyPair
from cloud_mgmt.models import Bill
from cloud_mgmt import tasks
import ipdb


class ProviderViewSet(viewsets.ModelViewSet):
    queryset = Provider.objects.all()
    serializer_class = ProviderSerializer

    def list(self, request):
        providers = Provider.objects.all()
        serializer = ProviderSerializer(providers, many=True)
        return Response(serializer.data)

    def create(self, request):
        try:
            provider = Provider.objects.get(access_key=request.data.get('access_key'))
        except ObjectDoesNotExist:
            provider = Provider()

        provider.name = request.data.get('name')
        provider.access_key = request.data.get('access_key')
        provider.secret_key = request.data.get('secret_key')
        provider.save()

        serializer = ProviderSerializer(provider)
        data = serializer.data
        response = {
            'success': True,
            'message': 'Provedor adicionado com sucesso',
            'provider': data
        }

        return Response(response)

    def retrieve(self, request, pk=None):
        pass

    def update(self, request, pk=None):
        pass

    def partial_update(self, request, pk=None):
        pass

    def destroy(self, request, pk=None):
        pass


class SecurityGroupViewSet(viewsets.ModelViewSet):
    queryset = SecurityGroup.objects.all()
    serializer_class = SecurityGroupSerializer

    def list(self, request):
        sgs = SecurityGroup.objects.all()
        serializer = SecurityGroupSerializer(sgs, many=True)
        return Response(serializer.data)


class KeyPairViewSet(viewsets.ModelViewSet):
    queryset = KeyPair.objects.all()
    serializer_class = KeyPairSerializer

    def list(self, request):
        key_pairs = KeyPair.objects.all()
        serializer = KeyPairSerializer(key_pairs, many=True)
        return Response(serializer.data)


class InstanceViewSet(viewsets.ModelViewSet):
    queryset = Instance.objects.all()
    serializer_class = InstanceSerializer

    def list(self, request):
        instances = Instance.objects.all()
        for instance in instances:
            instance.update_instance_data()
        serializer = InstanceSerializer(instances, many=True)
        return Response(serializer.data)

    def create(self, request):
        config = {
            'name': request.data.get('name'),
            'size_id': request.data.get('size_id'),
            'security_group': request.data.get('security_group'),
            'key_pair': request.data.get('key_pair')
        }
        provider = Provider.objects.get(pk=request.data['provider_id'])
        provider.create_instance(config)
        return Response({'success': True, 'message': 'Criando Instância'})

    def retrieve(self, request, pk=None):
        try:
            instance = Instance.objects.get(pk=pk)
            instance = instance.provider.update_instance_data(instance)
            return Response(InstanceSerializer(instance).data)
        except ObjectDoesNotExist:
            return Response({'error': True, 'message': "Instância não encontrada"})

    def update(self, request, pk=None):
        pass

    def partial_update(self, request, pk=None):
        pass

    def destroy(self, request, pk=None):
        try:
            instance = Instance.objects.get(pk=pk)
            response = instance.provider.destroy_instance(instance)
            return Response(response)
        except ObjectDoesNotExist:
            return Response({'error': True, 'message': "Instância não encontrada"})


@api_view(['POST'])
def test_provider_connection(request, pk):
    provider = Provider.objects.get(pk=pk)
    result = provider.test_connection()
    if result.get('success'):
        provider.last_connection_status = True
        provider.save()
    else:
        provider.last_connection_status = False
        provider.save()

    return Response(result)


@api_view(['GET'])
def get_available_sizes(request):
    sizes = [size[0] for size in Instance.AVAILABLE_SIZES]
    return Response(sizes)


@api_view(['POST'])
def sync_provider_instances(request, pk):
    provider = Provider.objects.get(pk=pk)
    provider.sync_nodes()
    provider.sync_security_groups()
    provider.sync_key_pairs()

    return Response({'success': True, 'message': 'Provedor sincronizado com sucesso'})


@api_view(['GET'])
def get_instances(request):
    providers = Provider.objects.all()
    nodes = []
    for provider in providers:
        nodes += provider.get_nodes()
    return Response(nodes)


@api_view(['POST'])
def start_instance(request, pk):
    tasks.start_node(pk)
    node = Instance.objects.get(pk=pk)
    serializer = InstanceSerializer(node)
    data = serializer.data

    return Response({
        'success': True,
        'message': "Iniciando Instância",
        'instance': data
    })


@api_view(['POST'])
def stop_instance(request, pk):
    tasks.stop_node(pk)
    node = Instance.objects.get(pk=pk)
    node.update_instance_data()
    serializer = InstanceSerializer(node)
    data = serializer.data

    return Response({
        'success': True,
        'message': "Desligando Instância",
        'instance': data
    })


@api_view(['POST'])
def resize_instance(request, pk):
    size_id = request.data.get('size_id')
    tasks.resize_node.delay(pk, size_id)
    node = Instance.objects.get(pk=pk)
    serializer = InstanceSerializer(node)
    data = serializer.data

    return Response({
        'success': True,
        'message': "Redimensionando Instância",
        'instance': data
    })


@api_view(['GET'])
def get_security_groups_by_provider(request, pk):
    sgs = SecurityGroup.objects.filter(provider__pk=pk)
    serializer = SecurityGroupSerializer(sgs, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def get_key_pairs_by_provider(request, pk):
    key_pairs = KeyPair.objects.filter(provider__pk=pk)
    serializer = KeyPairSerializer(key_pairs, many=True)
    return Response(serializer.data)


@api_view(['GET'])
def get_billing_data(request):
    providers = Provider.objects.all()
    data = []
    for provider in providers:
        bills = provider.bill.all().order_by('month')
        bills = BillSerializer(bills, many=True).data
        obj = {
            'provider': ProviderSerializer(provider).data,
            'bills': bills
        }
        data.append(obj)
    return Response(data)
