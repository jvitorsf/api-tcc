from django.core.management.base import BaseCommand, CommandError
from cloud_mgmt import tasks

class Command(BaseCommand):
    help = 'Schedules a task to be executed'

    def add_arguments(self, parser):
        parser.add_argument(
            '-w',
            type=str,
            dest='weekday',
            help='Specifies the weekdays the task should run comma separated (1,2,3...)',
        )
        parser.add_argument(
            '-t',
            type=int,
            dest='hour',
            default=0,
            help='Specifies the hour the task should run (0-23)',
        )
        parser.add_argument(
            '-m',
            type=int,
            dest='minute',
            default=0,
            help='Specifies the minute the task should run (0-59)',
        )

    def handle(self, *args, **options):
        import ipdb; ipdb.set_trace()
        # for poll_id in options['poll_id']:
        #     try:
        #         poll = Poll.objects.get(pk=poll_id)
        #     except Poll.DoesNotExist:
        #         raise CommandError('Poll "%s" does not exist' % poll_id)

        #     poll.opened = False
        #     poll.save()

        #     self.stdout.write(self.style.SUCCESS('Successfully closed poll "%s"' % poll_id))