# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2016-10-09 01:17
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('scheduler', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='name',
            field=models.CharField(default='', max_length=255, null=True),
        ),
    ]
