from django.db import models

# Create your models here.
class Task(models.Model):
    """
    Description: Store shceduled tasks
    """
    START = 'START_INSTANCE'
    STOP = 'STOP_INSTANCE'
    CREATE_SNAPSHOT = 'CREATE_SNAPSHOT'

    TASK_CHOICES = (
    	(START, 'Start Instance'),
    	(STOP, 'Stop Instance'),
    	(CREATE_SNAPSHOT, 'Create Snapshot')
	)

    name = models.CharField(max_length=255, null=True, default='')
    task_type = models.CharField(max_length=255, choices=TASK_CHOICES)
    time = models.TimeField(auto_now=False, auto_now_add=False, null=True, default=None)
    weekday = models.SmallIntegerField(null=True, default=None)