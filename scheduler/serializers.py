from rest_framework import serializers
from scheduler.models import Task
from djcelery.models import PeriodicTask

class TaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = ('id', 'name', 'task_type', 'weekday', 'time', 'repeat_task')


class PeriodicTaskSerializer(serializers.ModelSerializer):
    class Meta:
        model = PeriodicTask
        fields = ('id', 'name', 'task', 'enabled', 'last_run_at', 'total_run_count', 'description')
