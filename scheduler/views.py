from django.core.exceptions import ObjectDoesNotExist
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import api_view
from scheduler.models import Task as ModelTask
from .serializers import TaskSerializer, PeriodicTaskSerializer
from cloud_mgmt import tasks as cloud_mgmt_tasks
from djcelery.models import PeriodicTask, CrontabSchedule
from rest_framework.response import Response
import json
import dateutil.parser
import ipdb


class PeriodicTaskViewSet(viewsets.ModelViewSet):
    queryset = PeriodicTask.objects.all()
    serializer_class = TaskSerializer

    def list(self, request):
        tasks = PeriodicTask.objects.all()
        serializer = PeriodicTaskSerializer(tasks, many=True)
        return Response(serializer.data)

    def create(self, request):
        try:
            schedule_time = dateutil.parser.parse(request.data.get('datetime')).time()
            hour = str(schedule_time.hour)
            minute = str(schedule_time.minute)
        except ValueError:
            return Response({'error': True,
                             'message': 'Você precisa selecionar uma hora para que a tarefa seja executada'})
        config = {
            'days_of_month': '*',
            'days': request.data.get('days', []),
            'month': request.data.get('month', '*'),
            'weekday': request.data.get('weekday', []),
            'task': request.data.get('task', None),
            'task_name': request.data.get('name', ''),
            'instance_id': request.data.get('instance_id', None)
        }

        if config['instance_id']:
            weekdays_to_run = (', '.join(repr(day['code']) for day in config['days'] if day['run']))
            if len(weekdays_to_run) == 0:
                weekdays_to_run = '*'

            try:
                cron = CrontabSchedule.objects.get(
                    hour=hour,
                    minute=minute,
                    day_of_month=config['days_of_month'],
                    month_of_year=config['month'],
                    day_of_week=weekdays_to_run
                )
            except ObjectDoesNotExist:
                cron = CrontabSchedule()
                cron.minute = minute
                cron.hour = hour
                cron.day_of_month = config['days_of_month']
                cron.month_of_year = config['month']
                cron.day_of_week = weekdays_to_run
                cron.save()

            args = json.dumps([int(config['instance_id'])])
            task = PeriodicTask()
            task.name = config['task_name']
            task.crontab = cron
            task.enabled = True
            task.args = args

            if config['task'] == 'start_node':
                task.task = 'cloud_mgmt.tasks.start_node'
            elif config['task'] == 'stop_node':
                task.task = 'cloud_mgmt.tasks.stop_node'
            task.save()

            return Response({'success': True, 'message': 'Tarefa Agendada'})
        else:
            return Response({'error': True,
                             'message': 'Você precisa escolher uma instância para agendar a tarefa'})

    def retrieve(self, request, pk=None):
        pass

    def update(self, request, pk=None):
        task = PeriodicTask.objects.get(pk=pk)
        if task.enabled:
            task.enabled = False
        else:
            task.enabled = True

        task.save()
        return Response({'success': True, "message": "Tarefa Atualizada"})

    def partial_update(self, request, pk=None):
        pass

    def destroy(self, request, pk=None):
        task = PeriodicTask.objects.get(pk=pk)

        task.delete()
        return Response({'success': True, "message": "Tarefa Excluída"})


@api_view(['POST', 'DELETE'])
def schedule_task(request):
    if request.method == 'POST':
        config = {
            'minute': request.data.get('minute', '*'),
            'hour': request.data.get('hour', '*'),
            'day': request.data.get('day', '*'),
            'month': request.data.get('month', '*'),
            'weekday': request.data.get('weekday', '*'),
            'task': request.data.get('task', None),
            'task_name': request.data.get('task_name', '')
        }

        try:
            cron = CrontabSchedule.objects.get(
                minute=config['minute'],
                hour=config['hour'],
                day_of_month=config['day'],
                month_of_year=config['month'],
                day_of_week=config['weekday']
            )
        except ObjectDoesNotExist:
            cron = CrontabSchedule()
            cron.minute = config['minute']
            cron.hour = config['hour']
            cron.day_of_month = config['day']
            cron.month_of_year = config['month']
            cron.day_of_week = config['weekday']
            cron.save()

        args = json.dumps([int(request.data.get('node_id'))])
        task = PeriodicTask()
        task.name = config['task_name']
        task.crontab = cron
        task.enabled = True
        task.args = args

        if config['task'] == 'START_NODE':
            task.task = 'cloud_mgmt.tasks.start_node'
        elif config['task'] == 'STOP_NODE':
            task.task = 'cloud_mgmt.tasks.stop_node'
        task.save()

        return Response({'success': True, 'message': 'TASK_SCHEDULED'})