"""tcc URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from rest_framework.routers import DefaultRouter

router = DefaultRouter()

from cloud_mgmt.views import ProviderViewSet
from cloud_mgmt.views import InstanceViewSet
from cloud_mgmt.views import SecurityGroupViewSet
from cloud_mgmt.views import KeyPairViewSet
from scheduler.views import PeriodicTaskViewSet
from cloud_mgmt import views as cloud_mgmt_views
from scheduler import views as scheduler_views

router.register(r'providers', ProviderViewSet)
router.register(r'instances', InstanceViewSet)
router.register(r'tasks', PeriodicTaskViewSet)
router.register(r'security_groups', SecurityGroupViewSet)
router.register(r'key_pairs', KeyPairViewSet)

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^admin/', admin.site.urls),
    url(r'^providers/(?P<pk>[0-9]+)/test', cloud_mgmt_views.test_provider_connection),
    url(r'^providers/(?P<pk>[0-9]+)/sync', cloud_mgmt_views.sync_provider_instances),
    url(r'^providers/(?P<pk>[0-9]+)/security_groups', cloud_mgmt_views.get_security_groups_by_provider),
    url(r'^providers/(?P<pk>[0-9]+)/key_pairs', cloud_mgmt_views.get_key_pairs_by_provider),
    url(r'^instances/all', cloud_mgmt_views.get_instances),
    url(r'^instances/sizes', cloud_mgmt_views.get_available_sizes),
    url(r'^instances/(?P<pk>[0-9]+)/start', cloud_mgmt_views.start_instance),
    url(r'^instances/(?P<pk>[0-9]+)/stop', cloud_mgmt_views.stop_instance),
    url(r'^instances/(?P<pk>[0-9]+)/resize', cloud_mgmt_views.resize_instance),
    url(r'^bills/', cloud_mgmt_views.get_billing_data),
    url(r'^scheduler/', scheduler_views.schedule_task)
]
